# cr_server

Minimal server implementation for 
[Control-room](https://gitlab.com/simple-human/control_room) library with

- tls support via rustls
- nice configuration system via [libucl](https://github.com/vstakhov/libucl) 

## installation

```
git clone https://gitlab.com/simple-human/cr_server 
cd cr_server
# modify ./src/drivers.rs
cargo install --path .
```

## usage

cr\_server takes single mandatory argument - path to a working directory.
The directory should contain following files in ucl format:

- config,
- passwd,
- policy,
- sources,
- observables.

### config

```
address = localhost:9001
tls_key = ./tls/server.key
tls_cert = ./tls/server.crt
serve = ./front
```
Here

- *address* is ip:port to bind the server;
- *tls_key* and *tls_cert* are pathes to key and cert files;
- *serve* - an optional path to a web site directory to serve.

### passwd

This file contains a user:password db. Passwords should be hashed with
[Argon2](https://github.com/P-H-C/phc-winner-argon2).

```
test_username "$argon2i$v=19$m=4096,t=3,p=1$dGVlZWVlZWVzdA$7o/OC6/PSwq4UeVdTKHdfaCsbnWhHP9G9lntNe201Ys"
```

### policy

A set of subscription rules for groups and users.

```
groups {
    adm = [stats, logs]
    users = [data1,data2]
}
users {
    john {
        observables = [data3, data4],
        groups = [users]
    }
}

```

- *groups* is a list of groups. Each group is a collection of observables.
- *users* - a set of users with a lists of permited observables and attached groups.

### sources

```
echo {
    driver = sh_json
    options {
        cmd = "{command} | jq -R --slurp ."
    }
}

sqlite {
    driver = sqlite
    options {
        file = /tmp/test.db
    }
}

hub {
    driver = hub
    options {
        server = "remote_host:9001/control_room"
        user = test
        password = test
    }
}

```

A set of data sources. A source has

- *driver* - a driver name corresponding to `src/modules.rs` register.
- *options* - options for a driver. It depends on the particular driver.

### observables

The *observable* is a collection of *datasets* and a *client* config - client side configuration to properly render the data.

Each *dataset* has following fileds:

- *source* from *sources* configuration;
- *data_options* - optional server side config (for example a query for db request);
- *view_options* - optional client side options for this particular dataset;

*client* config has

- optional *title*;
- optional *description*;
- optional *options*, object to configure client side rendering;
- optional *controls*, a suggestion to client for default observable *controls* (query parameters for example).

```
date {
  datasets =  [{
    source = echo
  
    view_options {
      name = "Current date"
    }, 
    
    data_options {
      interval = 1 s
      args {
        command = date
      }
    }
  }],
  
  client {
    title = "Cmd test"
    description = "A test of cmd driver"
  }
}

sql {
  datasets = [{
  
    source = sqlite 
  
    view_options {
      name = temperature
    } 
  
    data_options {
      interval = 30 s 
      query = "select t as y, datetime(time_created, 'localtime') as x from temperature where time_created > datetime('now', :time) order by id desc limit 200"
    }
  
  }]
  
  client {
    title = temperature
    description = CPU temperature
  
    # default controls passed to subscription
    controls {
      time = -5 minutes
    }                  
  
    # options for client plotter library
    options {
      scales {
  
        xAxes = [{
          type = time
          time {
            offset = true
            stepSize = 30
            displayFormats {
              hour = "H"
              minute = "H:mm"
              second = "H:mm:ss"
              millisecond = "H:mm:ss.SSS"
            }
          }
        }]
  
        yAxes = [{
          ticks {
            suggestedMax = 100
            suggestedMin = 20
          }
        }]
      
      }
    }
  
  }
}

```
