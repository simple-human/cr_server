#![deny(clippy::all)]
use actix_web::*;
use control_room::{ControlRoom, Error};
use rustls::internal::pemfile::{certs, rsa_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use std::fs::File;
use std::path::PathBuf;
use std::io::BufReader;
use libucl::{Parser, Emitter};
use serde_derive::Deserialize;

mod modules;

#[derive(Deserialize, Clone)]
struct Config {
    address: String,
    tls_key: String,
    tls_cert: String,
    serve: Option<String>,
}

#[actix_web::main]
async fn main() -> Result<(), Error> {

    simple_logger::SimpleLogger::from_env().init().unwrap();

    let mut args = std::env::args();
    args.next();
    let arg = args.next().ok_or_else(|| Error::ServerError("missing configuration directory argument".into()))?;
    let working_dir = PathBuf::from(arg);
    if !working_dir.is_dir() {
        return Err(Error::ServerError("wrong config directory".into()));
    }
    std::env::set_current_dir(&working_dir)
        .map_err(|e| Error::ServerError(format!("wrong config directory: {}",e)))?;

    let drivers = modules::modules();
    
    let config: Config = load_config("config")?;
    let password_db = load_config("passwd")?;
    let policy = load_config("policy")?;
    let sources_config = load_config("sources")?;
    let observables = load_config("observables")?;

    let certificate =
        File::open(&config.tls_cert).map_err(|e| Error::TlsError(e.to_string()))?;
    let key = File::open(&config.tls_key).map_err(|e| Error::TlsError(e.to_string()))?;

    let mut cert = BufReader::new(certificate);
    let mut key = BufReader::new(key);

    let mut cert_chain = vec![];
    let mut server_config = ServerConfig::new(NoClientAuth::new());
    let mut cert =
        certs(&mut cert).map_err(|_| Error::TlsError("Error loading tls cert".into()))?;
    cert_chain.append(&mut cert);
    let mut keys =
        rsa_private_keys(&mut key).map_err(|_| Error::TlsError("Error loading tls key".into()))?;
    server_config
        .set_single_cert(cert_chain, keys.remove(0))
        .map_err(|_| Error::TlsError("Wrong tls keys".into()))?;

    let cr = ControlRoom {
        password_db,
        policy,
        drivers,
        sources_config,
        observables,
    };

    let serve = config.serve.clone();

    HttpServer::new(
        move || {
            let mut app = App::new()
                .service(cr.clone().scope().unwrap());
            if let Some(serve) = &serve {
                app = app.service(actix_files::Files::new("/", serve)
                    .index_file("index.html")
                    .show_files_listing());
            }
                    
            app
        }
    )
    .bind_rustls(&config.address, server_config)
    .unwrap()
    .run()
    .await
      .unwrap();
      Ok(())
}

fn load_config<T>(file: &str) -> Result<T, Error> 
    where T: serde::de::DeserializeOwned
{
    let config_parser = Parser::new();
    let config = std::fs::read_to_string(file)
        .map_err(|e| Error::ServerError(format!("{} format error: {}", file, e)))?;
    let ucl = config_parser.parse(config)
        .map_err(|e| Error::ServerError(format!("{} format error: {}", file, e)))?;
    let json = ucl.dump_into(Emitter::JSONCompact);

    serde_json::from_str(&json)
        .map_err(|e| Error::ServerError(format!("{} format error: {}", file, e)))
}
