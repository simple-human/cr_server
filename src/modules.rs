use control_room::DriverRegister;

pub fn modules() -> DriverRegister {
  let drivers: DriverRegister = Default::default();

  // source drivers configuration
  drivers.register::<cr_cmd_driver::CmdDriver>("sh_json".into());
  drivers.register::<cr_sqlite_driver::SqliteDriver>("sqlite".into());
  drivers.register::<cr_hub_driver::HubDriver>("hub".into());

  drivers
}
